#!/usr/bin/perl

use RINGS::Web::HTML;

$id = Get_Cookie_Info();
print "Content-type: text/html\n\n";

print <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html">

    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="/rings2.css" type="text/css">

    <title>GlycoCT (condensed) to KCF</title>
<script Language="JavaScript"><!--
function change(obj,col,ft)
{
	obj.style.backgroundColor = col;
	obj.style.color = ft;
}
// --></script>
</head>

<Body><!-- alink="#ff0000" bgcolor="#ffffff" link="#0000ef" text="#000000" vlink="#55188a" -->
<div id =" menu">

<p class="glow"><a href="/index.html"><img src="/rings4.png" border=0></a>
<font size=6>
GlycoCT (condensed) to KCF</font>
</div>
<hr>
<table width=1000 border=0 cellpadding=0 cellspacing=0>
<tr valign="top"><td width=200>
        <h4 style="text-align : center;" align="center">
        <a href="/index.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Home</a>
        </h4>

        <h4 style="text-align : center;" align="center">
        <a href="/help/help.html" target=_blank onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Help</a>
        </h4>

        <h4 style="text-align : center;" align="center">
EOF

my $bugTarget = "BugDisplay.pl";
if($id !=0){
   $bugTarget = "BugReportForm.pl?tool=GlycoCT to KCF";
}

### examples in a text area below is G00015 and G00044.

print <<EOF;
        <a href="/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#000033')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
        </h4>

</td>

<td width=750 border=0 cellpadding=0 cellspacing=0>
<form action="/cgi-bin/tools/utilities/GlycoCTtoKCF_au/glycoct_to_kcf_au.pl" method="POST" name="GlycoCTtoKCF" enctype="multipart/form-data">
<b><font size="3">Data set name</font></b>
<input type="text" name="datasetname" value="default">
<font size=2>
<Br><br>
Please enter a glycan structure in GlycoCT (condensed) format. 
"x" or "-1" can be used fore representing missing linkage information. 
Selective linkage information can be represented with "|". (i.e. a2-3 OR a2-6 linkage is represented as (3|6+2))<br>

<u>Note: please insert a blank line between each glycan strcuture.</u>
<Br><br>
</font>

<textarea name ="GlycoCT" rows="15" cols="80">
RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dman-HEX-1:5
6b:a-dman-HEX-1:5
7b:b-dglc-HEX-1:5
8s:n-acetyl
9b:a-dman-HEX-1:5
10b:b-dglc-HEX-1:5
11s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(3+1)6d
6:6o(2+1)7d
7:7d(2+1)8n
8:5o(6+1)9d
9:9o(2+1)10d
10:10d(2+1)11n

RES
1b:b-dglc-HEX-1:5
2b:b-dgal-HEX-1:5
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dgal-HEX-1:5
6b:a-lgal-HEX-1:5|6:d
LIN
1:1o(4+1)2d
2:2o(3+1)3d
3:3d(2+1)4n
4:3o(3+1)5d
5:5o(2+1)6d
</textarea>
<br>

Or load it from disk <input name="GlycoCTfile" type="file">
<P>

<input type="radio" name="type" value="html" checked>HTML
<input type="radio" name="type" value="text">TEXT
<p>
<input value="Clear" onclick="GlycoCTtoKCF.GlycoCT.value='',GlycoCTtoKCF.datasetname.value=''" type="button">
<input value="Get KCF" type="submit">

</CENTER>

</form>

</body>
</html>
EOF
