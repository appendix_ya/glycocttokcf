#!/usr/bin/perl

use XML::Simple;
#$XML::Simple::PREFERRED_PARSER = 'XML::SAX::ParserFactory';
$XML::Simple::PREFERRED_PARSER = 'XML::Parser';
#use Data::Dumper;
use RINGS::Web::HTML;
use RINGS::Glycan::KCF;
use RINGS::Web::user_admin;
use File::Copy;
use Socket;
use FileHandle;
use MIME::Base64;

require 5.001;
require "../../share/cgi-lib.pl";


my (%cgi_data,  # The form data
  %cgi_cfn,   # The uploaded file(s) client-provided name(s)
  %cgi_ct,    # The uploaded file(s) content-type(s).  These are
              #   set by the user's browser and may be unreliable
  %cgi_sfn,   # The uploaded file(s) name(s) on the server (this machine)
  $ret,       # Return value of the ReadParse call.       
  $buf,        # Buffer for data read from disk.
);
  $cgi_lib::writefiles = "/tmp";   
 
  $cgi_lib::maxdata = 2000000; 

  $ret = ReadParse(\%cgi_data,\%cgi_cfn,\%cgi_ct,\%cgi_sfn);
if ($cgi_data{'type'} eq 'html') {
	print "Content-type: text/html\n\n"; 
	print "<html>\n";
	print "<head>\n";
	$title = "GlycoCT to KCF";
	$head = Make_Head_From_Title2($title);
	$title = "Converted Results";
	$h1 = Make_H1_From_Header($title);
	print "$head\n";
	print "<script type=\"text/javascript\">\n";
	print "parent.data_tree.location.href = parent.data_tree.location.href;\n";
	print "</script>\n";
	print "</head>\n";
	print "<body>\n";
	print "$h1\n";
        print "<pre>";
} else {
	print "Content-type: text/plain\n\n"; 
} 

	
  if (!defined $ret) {
    &CgiDie("Error in reading and parsing of CGI input");
  } elsif (!$ret) {
    &CgiDie("Missing parameters\n",
	    "Please complete the form <a href='fup.html'>fup.html</a>.\n");
  } elsif (!defined $cgi_data{'GlycoCTfile'} or !defined $cgi_data{'GlycoCT'}) {
    &CgiDie("Data missing\n",
	    "Please complete the form <a href='fup.html'>fup.html</a>.\n");
  }


  open (UPFILE, $cgi_sfn{'GlycoCTfile'}) or 
    &CgiError("Error: Unable to open file $cgi_sfn{'GlycoCTfile'}: $!\n");
  $buf = "";    # avoid annoying warning message
  $file = "";
  while (read (UPFILE, $buf, 8192)) {
    $buf =~ s/</&lt;/g;
    $buf =~ s/>/&gt;/g;
    $file .= $buf;
  }
  close (UPFILE);
  unlink ($cgi_sfn{'GlycoCTfile'}) or
   &CgiError("Error: Unable to delete file",
             "Error: Unable to delete file $cgi_sfn{'GlycoCTfile'}: $!\n");
#  print &HtmlBot;

#============
# Get Use ID
#============
 my $id = Get_Cookie_Info();

 $cgi_lib::writefiles = $cgi_lib::writefiles;
 $cgi_lib::maxdata    = $cgi_lib::maxdata;
 my $time = time();
 $time = int(rand($time));
 mkdir ("/tmp/GlycoCTtoKCF$time") || die "failed $!";
 $get_glycoct = "";
	if(length $file > 0){
#		$file =~ s/\n\n/\n/g;
		$get_glycoct = $file;
	}else{
		$get_glycoct = $cgi_data{'GlycoCT'};
#		$get_glycoct =~ s/\n\n/\n/g;
	}
 $get_glycoct =~ s/&lt;/\</g;
 $get_glycoct =~ s/&gt;/\>/g;
 # save to file
 $filename = "/tmp/GlycoCTtoKCF$time/INPUT.xml";
 open(OUTCT,">$filename");
 print OUTCT $get_glycoct;
 close(OUTCT);

#-------------------------------------------------------------------sub
my $dbh = Connect_To_Rings();
my $dsname = $cgi_data{'datasetname'};
#===============
#Data Size
#===============
if($id != 0)
{
	#Get input data size
	$file = "/tmp/GlycoCTtoKCF$time/INPUT.xml";
	my $Input_data_size = -s $file;

	#Get output data size
	$file = "/tmp/GlycoCTtoKCF$time/OUTPUT.kcf";
	my $Output_data_size = -s $file;

	$Output_id = Insert_GlycoCTtoKCF_Data ($dbh, $dsname, $Input_data_size, $Output_data_size, $id);
}

sub Make_Dir
{
        my $SAVE_DIR = shift @_;
        if(!-d $SAVE_DIR)
        {
                mkdir ($SAVE_DIR) || die "failed: $!";
        }
}

#================
# Make Directory
#================
  my $SAVE_DIR = "/tmp";

  if($id !=0)
  {
        $SAVE_DIR = "/var/www/userdata/$id";
        Make_Dir($SAVE_DIR);
        $SAVE_DIR = "$SAVE_DIR/GlycoCT to KCF";
        Make_Dir($SAVE_DIR);
        $SAVE_DIR = "$SAVE_DIR/$Output_id";
        Make_Dir($SAVE_DIR);
  }

###########
#Move File#
###########
if($id !=0)
{
	$from_dir = "/tmp/GlycoCTtoKCF$time/";
	$to_dir = "/var/www/userdata/$id/GlycoCT to KCF/$Output_id/";

	move($from_dir."INPUT.xml",$to_dir."INPUT.xml");
	move($from_dir."OUTPUT.kcf",$to_dir."OUTPUT.kcf");
}

# print "<script type=\"text/javascript\">\n";
# print "parent.data_tree.location.href = parent.data_tree.location.href;\n";
# print "</script>\n";

sub getedge
{
	my $linkages = shift @_;

	foreach $linkages (@$linkages){
		print "$linkages->{id}\nP\t$linkages->{parent}\t$linkages->{linkage}[0]->{parent}[0]->{pos}\nC\t$linkages->{child}\t$linkages->{linkage}[0]->{child}[0]->{pos}\n";
	}
}

sub getanomer
{
	my $basetypes = shift @_;

	my %anomers = ();

	foreach $id (sort keys %$basetypes) {
		my $idnum;
		my $anomer;
		if ($id =~ /^\d+$/) {
			$idnum = $id;
			$anomer = $basetypes->{$id}->{anomer};
		}else{
			$idnum = $basetypes->{id};
			$anomer = $basetypes->{anomer};
		}
		$anomers{$idnum} =$anomer;
	}
	my %ANOM = ();

	foreach $id (sort { $a <=> $b } keys %anomers){
		if($anomers{$id} =~ /a/){
			$ANOM{$id} = $anomers{$id};
		}elsif($anomers{$id} =~ /b/){
			$ANOM{$id} = $anomers{$id};
		}
	}
	return %ANOM;
}

sub getnode
{
	my $basetypes = shift @_;

	my %nodes = ();

	foreach $id (sort keys %$basetypes) {
		my $idnum, $name;
		if ($id =~ /^\d+$/) {
			$idnum = $id;
			$name = $basetypes->{$id}->{name};
		}else{
			$idnum = $basetypes->{id};
			$name = $basetypes->{name};
		}
		$nodes{$idnum} = $name;
	}
	my %NODE = ();
	foreach $id (sort keys %nodes) {
		if ($nodes{$id} =~ /\w-\w+-\w+-NON/) {
			substr($nodes{$id},0,2) = "";
			$NODE{$id} = $nodes{$id};
		}else{
			$atta = substr($nodes{$id},3,3);
			$NODE{$id} = $atta;
		}	

	}
	return %NODE;
}

sub getHaba
{
	my $SUM = 0;
	my ($node) = @_;
	if(defined $oyako{$node}){

		my @kids = @{$oyako{$node}};
		foreach $k (@kids){
			&getHaba($k);
			foreach $c (@kids){
				$SUM += $Haba{$c};
			}
		}
		my $W = @kids;
		$Haba{$node} = $SUM + ($W -1)*2;
	}else{
		$Haba{$node} = 0;
	}
}



sub getY
{
	my ($node) = @_;
	if(!defined $kooya{$node}){
		my $rot = $node;
		$cY{$rot} = 0;
	}
	if(defined $oyako{$node}){
		my @kids = @{$oyako{$node}};
		foreach $k (@kids){
			my $W = @kids;
			if($W == 1){
				$Cnum{$kids[0]} = 0;
			}elsif($W == 2){
				$Cnum{$kids[0]} = 0;
				$Cnum{$kids[1]} = 1;
			}elsif($W == 3){
				$Cnum{$kids[0]} = 0;
				$Cnum{$kids[1]} = 1;
				$Cnum{$kids[2]} = 2;
			}
			if($W == 1){
				$cY{$k} = $cY{$kooya{$k}};
			}else{
				$cY{$k} = $cY{$kooya{$k}}+($Haba{$kooya{$k}}/2)-$Cnum{$k}*($Haba{$kooya{$k}}/($W-1));
			}
			&getY($k);
		}
	}
}


sub getX
{
	my($node) = @_;
	my $XPS = 8;
	if(!defined $kooya{$node}){
		my $rot = $node;
		$cX{$rot} = 0;
	}
	if(defined $oyako{$node}){
		my @kids = @{$oyako{$node}};
		foreach $k (@kids){
			$cX{$k} = $cX{$kooya{$k}} - $XPS;
			&getX($k);
		}
	}
}



my $file_nodes = "/matlab_program/transNODEs_uniq.txt";  #unicarb condense file
### If you use Monosaccharide DB URL to transform a monosaccharide one by one, it really takes time!
### This file is a list monosaccharides in Monosaccharide DB and KCF format (ONLY symbols). 
### Should this data be add to RINGS Database?

my %uniqNODE = ();
open(IN, $file_nodes);
while (<IN>){
   my $line = $_;
   if($line =~ /^([\S]+)\s+([^\s]+)\n$/){
      $uniqNODE{$1} = $2;
   }
}

my $mode = -1;
my (%ress, %lins, %carbs, $unicarbs, $kcfs, $disaps, $uniLINES);
my $numKCF = 1;
my $numUNI = 1;
my $numIN = 0;
$number = 0;

$get_glycoct .= "\nGlycoCTend";
my @inputarray = split(/\n/, $get_glycoct);

foreach my $line (@inputarray){
   $uniLINES .= $line;
   $number += 1;

   if($line =~ /^RES/ && $mode == -1){
      $mode = 1;
      $numIN += 1;
   }elsif($line =~ /^LIN/ && $mode == 1){
      $mode = 2;
   }elsif($line =~ /^(UND|ALT|REP)[\n\s]+$/ && $mode == 2){
      $mode = 3;
   }elsif(($line =~ /^[\s\n]+$/ || $line =~ /GlycoCTend/) && $mode != 3){
      $kcfs .=  main(\%ress, \%carbs, \%uniqNODE, \%lins, $numKCF);
      $mode = -1;
      (%ress, %lins, %carbs) = ();
      $uniLINES = "";
      $numKCF += 1;
   }elsif($line =~ /^[\n\s]+$/ && $mode == 3){
      $disaps .= $uniLINES."\n";
      $mode = -1;
      (%ress, %lins, %carbs) = ();
      $uniLINES = "";
      $numUNI += 1;
   }


   if($line =~ /^(\d+[bs]):(.+)[\s\n]+$/ && $mode == 1){
      $ress{$1} = $2;
   }elsif($line =~ /^\d+[^bs]:.+$/ && $mode == 1){
      print STDERR "Check! There is another letter out of b and s at RES section\n$line";
      print STDERR "<input data> line around (before): $number\n";
      exit;
   }elsif($line =~ /^(\d+):(\d+d\(.+\)\d+n)/ && $mode == 2){
      $carbs{$1} = $2;
   }elsif($line =~ /^(\d+):(\d+o\(.+\)\d+[dn])/ && $mode == 2){
      $lins{$1} = $2;
   }
}




sub main{
   my ($ressREF, $carbsREF, $uniqNODEREF, $linsREF, $numkcf) = @_;
   my ($pre_nodesREF, $change_nidsREF, $nodeREF, $edgeREF, $rootnode, $oyakoREF, $xHashREF, $yHashREF, $habaHashREF, $kcf);


   ($pre_nodesREF, $change_nidsREF, $uniqNODEREF) = getKCFnodes($ressREF,$carbsREF,$uniqNODEREF);

   ($nodeREF, $edgeREF, $rootnode, $oyakoREF) = makeHashes4XY($pre_nodesREF,$change_nidsREF,$linsREF);

   my %xHash = ();
   my %yHash = ();
   my %habaHash = ();
   $xHash{$rootnode} = 0;
   $yHash{$rootnode} = 0;

   ($oyakoREF, $nodeREF, $xHashREF, $habaHashREF) = getX($rootnode, $oyakoREF, $nodeREF, \%xHash, \%habaHash);
   my ($id, $oyakoRef, $nodesRef, $xHashRef, $habaHashRef) = @_;
   ($oyakoREF, $nodeREF, $yHashREF, $habaHashREF) = getY($rootnode, $oyakoREF, $nodeREF, \%yHash, $habaHashREF);

   $kcf = makeKCF($rootnode, $nodeREF, $edgeREF, $xHashREF, $yHashREF, $numkcf);
   return $kcf;
}


sub getKCFnodes{
   my ($ressREF, $carbsREF, $uniqNODEREF) = @_;
   my %ress = %{$ressREF};
   my %carbs = %{$carbsREF};
   my %uniqNODE = %{$uniqNODEREF};
   my %pre_nodes = ();
   my %change_nids = ();
   my $num = 1;

   foreach my $key (keys %carbs){
      my $monosaccharide = "";
      my $substi = $carbs{$key};
      if($substi =~ /(\d+)d\((.)\+(.)\)(\d+)n/){
         my $parent = $1;
         my $parentRES = $parent."b";
         my $parentBOND = $2;
         my $childBOND = $3;
         my $childRES = $4."s";

         my $saccharide = $ress{$parentRES}."||(".$parentBOND."d:".$childBOND.")".$ress{$childRES};

         if(exists $uniqNODE{$saccharide}){
            $monosaccharide = $uniqNODE{$saccharide};
            $pre_nodes{$num} = $monosaccharide;
            $change_nids{$parent} = $num;
            $num += 1;
         }else{
            my $url = "http://www.monosaccharidedb.org/convert_residue.action?sourceScheme=msdb&targetScheme=carbbank&name=$saccharide";
            ($http_response, $errorMessage) = getHTTP($url);

            if($http_response =~ /<monosaccharide_name>(.+)<\/monosaccharide_name>/){
               $monosaccharide = $1;
               if(length $monosaccharide != 0){
                  $pre_nodes{$num} = $monosaccharide;
                  $change_nids{$parent} = $num;
                  $num += 1;
                  $uniqNODE{$saccharide} = $monosaccharide;
                  print STDERR "KCF NODE => $monosaccharide\nUnicarb NODE => $saccharide\nUnicarb RES ID => $key\n\n";
                  print STDERR "<input data> line around (before): $number\n";
               }else{
                  print STDERR "*** Monosaccharide is EMPTY!! ***\n";
                  print STDERR "ERROR ---- $errorMessage\n\n";
                  print STDERR "<input data> line around (before): $number\n";
               }

            }elsif(length $errorMessage != 0){
               print STDERR "ERROR at $saccharide ---- $errorMessage\n\n";
               print STDERR "<input data> line around (before): $number\n";
            }
         }

         delete $ress{$parentRES};
         delete $ress{$childRES};
      }
   }

   foreach my $key (keys %ress){
      my $monosaccharide = "";
      my $saccharide = $ress{$key};

      if($key =~ /(\d+)s/){
         $change_nids{$1} = $num;
         $pre_nodes{$num} = $uniqNODE{$saccharide};
         $num += 1;
      }elsif($key =~ /(\d+)b/ && exists $uniqNODE{$saccharide}){
         $parent = $1;
         $monosaccharide = $uniqNODE{$saccharide};

         $change_nids{$parent} = $num;
         $pre_nodes{$num} = $monosaccharide;
         $num += 1;
      }elsif($key =~ /(\d+)b/ && !(exists $uniqNODE{$saccharide})){
         $parent = $1;
         my $url = "http://www.monosaccharidedb.org/convert_residue.action?sourceScheme=msdb&targetScheme=carbbank&name=$saccharide";
         ($http_response, $errorMessage) = getHTTP($url);

         if($http_response =~ /<monosaccharide_name>(.+)<\/monosaccharide_name>/){
            $monosaccharide = $1;
            if(length $monosaccharide != 0 and $key =~ /(\d+)\w/){
               $change_nids{$1} = $num;
               $pre_nodes{$num} = $monosaccharide;
               $num += 1;

               $uniqNODE{$saccharide} = $monosaccharide;
               print STDERR "1) Now we are transforming Unicarb NODE => $saccharide\nUnicarb RES ID => $key\n";
               print STDERR "\tinto KCF NODE => $monosaccharide\n";
               print STDERR "<input data> line around (before): $number\n";
            }else{
               print STDERR "*** Monosaccharide is EMPTY!! ***\n";
               print STDERR "ERROR ---- $errorMessage\n\n";
               print STDERR "<input data> line around (before): $number\n";
            }
         }elsif(length $errorMessage != 0){
            print STDERR "ERROR at $saccharide ---- $errorMessage\n\n";
            print STDERR "<input data> line around (before): $number\n";
         }
      }elsif($key =~ /\d+[nra]/){
         print STDERR "Check at $saccharide ---- There is another charactor than b and s\n\n";
         print STDERR "<input data> line around (before): $number\n";
      }
   }

   return(\%pre_nodes, \%change_nids, \%uniqNODE);
}



sub makeHashes4XY {
   my ($pre_nodesREF, $change_nidsREF, $linsREF) = @_;
   my %pre_nodes = %{$pre_nodesREF}; 
   my %change_nids = %{$change_nidsREF};
   my %lins = %{$linsREF};
   my %pre_edges = ();
   my %oyako = ();
   my %kooya = ();
   my $rootnode = -1;
   my $count = 0;

   foreach my $key (sort keys %lins){
      my ($resP_num, $resC, $linP, $linC_num, $resP, $resC);

      if($lins{$key} =~ /(\d+)o\((\d\|?\d?|x|-1)\+(\d|x|-1)\)(\d+)[dn]/){
         $resP_num = $change_nids{$1};
         $linP = $2;
         $linC = $3;
         $resC_num = $change_nids{$4};

         if($linP eq "x" || $linP == -1){
            $linP = " ";
         }elsif($linP =~ /(\d)\|(\d)/){
            $linP = $1.",".$2;
	     }
         $linC = " " if ($linC eq "x" || $linC == -1);

         $resC = $pre_nodes{$resC_num};
         if($resC =~ /^([ab])-([A-Z].*)$/){
            my $ab = $1;
            my $node = $2;
            $pre_nodes{$resC_num} = $node;
            my $edge = $resC_num;
	        if(($ab eq "" || $ab =~ /^\s+$/) && ($linC eq "" || $linC =~ /^\s+$/)){
               $edge .= "     ";
	        }else{
               $edge .= ":".$ab.$linC."     ";
	        }
	        if($linP eq "" || $linP =~ /^\s+$/){
               $edge .= $resP_num;
	        }else{
               $edge .= $resP_num.":".$linP;
	        }
	

            $count += 1;
            $pre_edges{$count} = $edge;

         }elsif($resC =~ /^[A-Z].*$/){
           # my $edge = $resC_num.":".$linC."     ".$resP_num.":".$linP;
            my $edge = $resC_num;
	        if ($linC eq "" || $linC =~ /^\s+$/){
               $edge .= "     ";
	        }else{
               $edge .= ":".$linC."     ";
	        }
	        if($linP eq "" || $linP =~ /^\s+$/){
               $edge .= $resP_num;
	        }else{
               $edge .= $resP_num.":".$linP;
	        }

            $count += 1;
            $pre_edges{$count} = $edge;
         } else {
	  	    #print "Couldn't parse $resC\n";
            my $edge = $resC_num.":".$linC."     ".$resP_num.":".$linP;
            $count += 1;
            $pre_edges{$count} = $edge;
	     }


         if(exists $oyako{$resP_num}){
            my @children = @{$oyako{$resP_num}};
            push(@children, $resC_num);
            $oyako{$resP_num} = [@children];
         }else{
            my @children = ($resC_num);
            $oyako{$resP_num} = [@children];
         }

         if(exists $kooya{$resC_num}){
            print STDERR "Error: there are multi Parent for one child\n";
            print STDERR "\tchild: $pre_nodes{$resC_num}\n";
            print STDERR "\tparents: $pre_nodes{$resP_num} and $kooya{$resC_num}\n";
            print STDERR "<input data> line around (before): $number\n";
         }else{
            $kooya{$resC_num} = $resP_num;
         }
      }
   }
   my $numedges = keys %pre_edges;
   #print "Num edges: $numedges\n";

   my @oya = keys %oyako;
   my @ko = keys %kooya;
   my %tmp_list = ();

   foreach my $item (@ko){
      $tmp_list{$item} = 1;
   }
   foreach my $item (@oya){
      if(!(exists $tmp_list{$item})){
         $rootnode = $item;
      }
   }

   if($rootnode == -1){
      print STDERR "Error: rootnode is -1\n";
      print STDERR "<input data> line around (before): $number\n";
   }

   return(\%pre_nodes, \%pre_edges, $rootnode, \%oyako);
}


sub getX {
   my ($id, $oyakoRef, $nodesRef, $xHashRef, $habaHashRef) = @_;
   my %oyako = %{$oyakoRef};
   my %nodes = %{$nodesRef};
   my %xHash = %{$xHashRef};
   my %habaHash = %{$habaHashRef};
   my @children = ();
   if (exists $oyako{$id}) {
      @children = @{$oyako{$id}};
   }
   my $numChildren = @children;
   if ($numChildren == 0) {
      $habaHash{$id} = 0;
   }
   my $habaSum = 0;
   foreach my $child (@children) {
      $xHash{$child} = $xHash{$id} - 8;
      ($oyakoRef, $nodesRef, $xHashRef, $habaHashRef) = getX($child,  \%oyako, \%nodes, \%xHash, \%habaHash);
      %oyako = %{$oyakoRef};
      %nodes = %{$nodesRef};
      %xHash = %{$xHashRef};
     %habaHash = %{$habaHashRef};
      $habaSum += $habaHash{$child};
   }
   if ($numChildren > 0) {
      $habaHash{$id} = ($numChildren-1) * 2 + $habaSum;
   }
   return (\%oyako, \%nodes, \%xHash, \%habaHash);
}


sub getY {
   my ($id, $oyakoRef, $nodesRef, $yHashRef, $habaHashRef) = @_;
   my %oyako = %{$oyakoRef};
   my %nodes = %{$nodesRef};
   my %yHash = %{$yHashRef};
   my %habaHash = %{$habaHashRef};
   my @children = ();
   if (defined $oyako{$id}) {
      @children = @{$oyako{$id}};
   }
   my $numChildren = @children;
   if ($numChildren == 1) {
      $yHash{$children[0]} = $yHash{$id};
   } elsif ($numChildren == 2) {
      $yHash{$children[0]} = $yHash{$id}-$habaHash{$id};
      $yHash{$children[1]} = $yHash{$id}+$habaHash{$id};
   } elsif ($numChildren == 3) {
      $yHash{$children[0]} = $yHash{$id}-$habaHash{$id};
      $yHash{$children[1]} = $yHash{$id};
      $yHash{$children[2]} = $yHash{$id}+$habaHash{$id};
   }
   foreach my $child (@children) {
      ($oyakoRef, $nodesRef, $yHashRef, $habaHashRef) = getY($child, \%oyako, \%nodes, \%yHash, \%habaHash);
      %oyako = %{$oyakoRef};
      %nodes = %{$nodesRef};
      %yHash = %{$yHashRef};
      %habaHash = %{$habaHashRef};
   }
   return (\%oyako, \%nodes, \%yHash, \%habaHash);
}



sub makeKCF {
   my ($rootnode, $nodesRef, $edgesRef, $xHashRef, $yHashRef, $numkcf) = @_;
   my %nodes = %{$nodesRef};
   my %edges = %{$edgesRef};
   my %xHash = %{$xHashRef};
   my %yHash = %{$yHashRef};
   my $result = "ENTRY         CT-".$numkcf."             Glycan\n";
   my $numNodes = keys %nodes;
   $result .= "NODE  $numNodes\n";
   foreach my $key (sort {$a <=> $b} keys %nodes) {
      if($key == $rootnode && $nodes{$key} =~ /^[abox]-(.+)$/){
         $nodes{$key} = $1;
      }
         my $tmp_nd= $nodes{$key};
      $result .= "     ".$key."  ".$nodes{$key}."   ".$xHash{$key}."   ".$yHash{$key}."\n";
   }
   my $numEdges = keys %edges;
   $result .= "EDGE  $numEdges\n";
   foreach my $key (sort {$a <=> $b} keys %edges) {
      $result .= "     ".$key."  ".$edges{$key}."\n";
   }
   $result .= "///\n";
   return $result;
}




sub getHTTP{
        local $\ = "";

        my %arg = ( @_ );
        my $data;
        my ( $uri, $proxy_address, $proxy_port, $http_version, $user_id, $password,
                $proxy_user_id, $proxy_password, $referrer, $referer, $agent ) =
        (
                $arg{URL}           || $_[0],
                $arg{Proxy}         || "",
                $arg{ProxyPort}     || "8080",
                $arg{HTTP}          || "1.1",
                $arg{UserID}        || "",
                $arg{Password}      || "",
                $arg{ProxyUserID}   || "",
                $arg{ProxyPassword} || "",
                $arg{Referrer}      || "",
                $arg{Referer}       || "",
                $arg{UserAgent}     || "HTTP client version 1.02 (www.kasai.fm)"
        );

        my ( $scheme, $domain, $server_address, $server_port, $path );
        my ( $target_address, $target_port, $target_path, $target_ip );


######### URI parsing

        ### retrieve scheme ( "http://" )

        if ( $uri =~ s!^(\w+?)://!! ){
                $scheme = $1;

                return ("",
                        "Can't handle '$scheme'. Only 'http' is possible"
                ) if ( $scheme !~ /^http$/i );
        }
        else{
                $scheme = 'http';
        }


        ### retrieve domain, port and path ( "hogehoge:8080/foo/bar.html" )

        ( $domain, $path ) = split( /\//, $uri, 2 );
        ( $server_address, $server_port ) = split( /:/, $domain, 2 );

        $server_address ||= "localhost";
        $server_port    ||= getservbyname( $scheme, "tcp" );


        ### complete info about your proxy server
        if ( $proxy_address and !$proxy_port ){
                ( $proxy_port ) = $proxy_address =~ /:(\d+)/;

                return ("",
                        "Proxy port is undefined.".
                        "Specify 'ProxyPort'=>8080 or 'Proxy'=>'${proxy_address}:8080'"
                ) unless ( $proxy_port );
        }


        ### switch arguments according to if you use a proxy server

        ( $target_address, $target_port, $target_path ) = $proxy_address ?
                (
                        $proxy_address,
                        $proxy_port,
                        "${scheme}://${server_address}:${server_port}/${path}"
                ) :
                (
                        $server_address,
                        $server_port,
                        "/$path"
                );


 ######### SOCKET (create)

        $target_ip    = inet_aton( $target_address ) || return ("", "Can't connect to $target_address" );
        $sock_address = pack_sockaddr_in( $target_port, $target_ip );

        socket(SOCKET, PF_INET, SOCK_STREAM, 0) || return ("", "Can't create socket on $target_address");


 ######### SOCKET (connect)

        connect(SOCKET, $sock_address) or return ("", "Can't connect socket on $sock_address");
        autoflush SOCKET (1);


 ######### Send HTTP GET request

        if ( $http_version eq "1.1" ) {
                print SOCKET "GET $target_path HTTP/1.1\n";
                print SOCKET "Host: $target_address\n";
                print SOCKET "Connection: close\n";
        }
        else {
                print SOCKET "GET $target_path HTTP/1.0\n";
        }

        if ( $user_id ){
                print SOCKET "Authorization: Basic\n ";
                print SOCKET &base64'b64encode("${user_id}:${password}")."\n";
        }

        if ( $proxy_user_id ){
                print SOCKET "Proxy-Authorization: Basic\n ";
                print SOCKET &base64'b64encode("${proxy_user_id}:${proxy_password}")."\n";
        }
        print SOCKET "User-Agent: $agent\n"  if ( $agent    );
        print SOCKET "Referrer: $referrer\n" if ( $referrer );
        print SOCKET "Referer: $referer\n"   if ( $referer  );


        print SOCKET "Accept: text/html; */*\n";
        print SOCKET "\n";


        ######### Receive HTTP response via SOCKET

        while ( <SOCKET> ) {
                chomp;
                $data .= "$_\n";
        }


        ######### SOCKET (close); take down the session

        close(SOCKET);


        ######### return

        return ($data, "");
}


open OUTPUT, '>', "/tmp/GlycoCTtoKCF$time/OUTPUT.kcf" or  die "failed $!";
print $kcfs;
print OUTPUT $kcfs;
close(OUTPUT);






